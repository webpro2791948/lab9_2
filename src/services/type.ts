import type { Type } from '@/types/Type'
import http from './http'

function addType(type: Type) {
  return http.post('/Types', type)
}

function updateType(type: Type) {
  return http.patch(`/Types/${type.id}`, type)
}

function delType(type: Type) {
  return http.delete(`/Types/${type.id}`)
}

function getType(id: number) {
  return http.get(`/Types/${id}`)
}

function getTypes() {
  return http.get('/Types')
}

export default { addType, updateType, delType, getType, getTypes }
